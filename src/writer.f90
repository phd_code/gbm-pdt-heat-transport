module writer_mod

   implicit none

   contains
      subroutine writer(temp)
      !   write out arrays
      !
      !
         use constants, only : nxg, nyg, nzg, fileplace
         use iarray,    only : jmeanGLOBAL, rhokap, temp_slice,heatcap, temp_slice_max, block_ua, max_temp_time
        ! use heat,      only : energyPerPixel, power
         use utils,     only : str

         implicit none

         real,    intent(IN) :: temp(0:nxg+1,0:nyg+1,0:nzg+1)
     

         integer :: u



         !open(newunit=u,file=trim(fileplace)//"jmean_run8.dat" &
         ! ,access="stream",form="unformatted", status="replace")
         !write(u)jmeanGLOBAL
         !close(u)

        ! open(newunit=u,file=trim(fileplace)//"jmean_slice_run1.dat" &
         ! ,access="stream",form="unformatted", status="replace")
         !write(u)jmean_slice
         !close(u)

         open(newunit=u,file=trim(fileplace)//"temp_slice_run73NB.dat" &
         ,access="stream",form="unformatted", status="replace")
        write(u)temp_slice - 273.d0
        close(u)

        open(newunit=u,file=trim(fileplace)//"temp_slice_max_run73NB.dat" &
         ,access="stream",form="unformatted", status="replace")
        write(u)temp_slice_max - 273.d0
        close(u)

         open(newunit=u,file=trim(fileplace)//"temp_rhokap_run73NB.dat" &
          ,access="stream",form="unformatted", status="replace")
         write(u)rhokap(1:nxg, 1:nyg, 1:nzg)
         close(u)

         open(newunit=u,file=trim(fileplace)//"temp_run73NB.dat" &
          ,access="stream",form="unformatted", status="replace")
         write(u)temp - 273.d0
         close(u)

         open(newunit=u,file=trim(fileplace)//"absorb.dat" &
          ,access="stream",form="unformatted", status="replace")
         write(u)block_ua
         close(u)

         open(newunit=u,file=trim(fileplace)//"temp_jmean_run73NB.dat" &
          ,access="stream",form="unformatted", status="replace")
         write(u)jmeanGLOBAL
         close(u)

         open(newunit=u,file=trim(fileplace)//"max_temp_time_run73NB.dat" &
         ,access="stream",form="unformatted", status="replace")
        write(u) max_temp_time - 273.d0
        close(u)


      end subroutine writer
end module writer_mod
