MODULE iarray
!
!  Contains all array var names.
!
    implicit none
    save

    real, allocatable :: xface(:),yface(:),zface(:)
    real, allocatable :: rhokap(:,:,:), albedoar(:,:,:)
    real, allocatable :: jmean(:,:,:),jmeanGLOBAL(:,:,:)
    !real, allocatable :: jmean_on(:,:,:), jmean_off(:,:,:)
    real, allocatable :: temp(:,:,:), tissue(:,:,:), tempGLOBAL(:,:,:), temp_in(:,:,:)
    real, allocatable :: ThresTime(:,:,:,:), ThresTimeGLOBAL(:,:,:,:)
    real, allocatable:: block_rho(:,:,:), block_alb(:,:,:), block_jmean(:,:,:)!, brain_block(:,:,:), brain_block_a(:,:,:)
    !real, allocatable:: rho_full(:,:,:), alb_full(:,:,:)

    real, allocatable:: temp_slice(:,:,:), block_ua(:,:,:), temp_slice_max(:,:,:)

    real, allocatable:: coeff(:,:,:),alpha(:,:,:),density(:,:,:), kappa(:,:,:)
    real, allocatable:: heatCap(:,:,:), Q(:,:,:), watercontent(:,:,:), max_temp_time(:)

end MODULE iarray
